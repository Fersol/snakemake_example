import pandas as pd

for i in range(len(snakemake.input)):
    data = pd.read_csv(snakemake.input[i])
    data.to_csv(snakemake.output[i], index=False)
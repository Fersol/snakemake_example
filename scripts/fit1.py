import pandas as pd
from sklearn.linear_model import Ridge
import pickle

for i in range(len(snakemake.input)):
    df = pd.read_csv(snakemake.input[i])
    y = df["ltv"].to_numpy()
    X = df.drop(columns=["ltv"]).to_numpy()
    model = Ridge()
    model.fit(X,y)
    with open(snakemake.output[i], "wb") as f:
        pickle.dump(model, f)
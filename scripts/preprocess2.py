import pandas as pd
import numpy as np

for i in range(len(snakemake.input)):
    df = pd.read_csv(snakemake.input[i])
    df = df.drop(columns=["user"])
    for column in df.columns:
        if df.dtypes[column] == "int64" and column != "ltv":
            df[column] = df[column].apply(lambda x: max(x,0))
    df.to_csv(snakemake.output[i], index=False)

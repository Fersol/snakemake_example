import pandas as pd
import numpy as np

np.random.seed(113)
N = 100
df = pd.DataFrame()
df["user"] = [f"user{i}" for i in range(N)]
df["ltv1"] = np.random.normal(20, 15, size=N).astype(int)
df["ltv2"] = np.random.normal(60, 40, size=N).astype(int)
df["ltv3"] = np.random.normal(80, 15, size=N).astype(int)
df["ltv4"] = np.random.normal(100, 30, size=N).astype(int)
df["ltv"] =  2 * df["ltv1"].apply(lambda x: max(x,0)) + 1.5 * df["ltv2"].apply(lambda x: max(x,0)) + np.random.normal(0, 7, size=N)

df.to_csv("data_in/ml.csv", index=False)
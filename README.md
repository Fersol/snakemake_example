# Snakeman example
На основе https://mlops.ml-devs.com/11.workflow_snakemake/workflow-snakemake.html#/title-slide

## Описание
Пример работы с инструментов Snakemake для построения workflow и pipeline-ов для ML задач.

Решаемая задача:
Задание по блоку Snakemake
- Реализовать пайплайн обработки данных при помощи snakemake, минимум 3 правила - чтение данных, препроцессинг данных, обучение модели.
- Добавить как минимум ещё 1 вариант препроцессинга и ещё 1 вариант обучения модели, настроив правила таким образом, чтобы в результате работы пайплана получились минимум 4 артефакта - комбинация двух моделей с двумя разными препроцессингами.
- Обобщить хотя бы одно правило, используя named wildcards.
- Воспользоваться хотя бы в одном правиле вспомогательной функцией expand.
- Добавить readme или отчёт в формате md/qmd, с описанием шагов вашего пайплайна и отображением dag, полученного при помощи внутренних функций snakemake.
- Использовать ограничение правил по ресурсам, например, параметр threads до половины числа ядер машины.

## Начало
Для работы необходимо установить пакет python3 -- snakemake.

Для этого в созданном окружении conda нужно выполнить команду:
```
conda install -c conda-forge -c bioconda snakemake
```

## Первые шаги
Файл snakefile содержит pipeline для сортировки датасета с правилом `sort`. Его запустить можно командой из директории, где находится файл snakefile.
```
snakemake sort --cores 1
```
P.S. --cores добавлены в связи с ошибкой 
```
Assuming unrestricted shared filesystem usage.
Error: cores have to be specified for local execution (use --cores N with N being a number >= 1 or 'all')
```
После выполнения появится новая папка data_out с выполненной командой. А так же папка .snakemake с логами и дополнительной информацией по запуску.

## Визуализация DAG-a
Необходима утилита dot (Graphviz) - https://graphviz.org/download/ 
```
snakemake sort --dag | dot -Tsvg > images/dag.svg
```

## ML pipeline
Будем решать задачу прогноза LTV на синтетических данных. Скрипт synthetic.py создает специальный датафрейм ml.csv в папке data_in.
Запуск синтеза:
```
python scripts/synthetic.py
```
Таргет переменная -- ltv. Остальное -- признаки. Только train выборка.

Правило по простейшей загрузке данных реализовано через скрипт, можно запустить по правилу read:
```
snakemake read --cores all
```

Запустить всю модель можно командой (в правило all задаются файлы, которые должны быть получены):
```
snakemake all --cores all
```

Отрисовка DAG-а
```
snakemake all --dag | dot -Tsvg > images/dag_main.svg
```
или для png
```
snakemake all --dag > images/dag_main.dot
dot -Tpng images/dag_main.dot -o images/dag_main.png
```
На Windows вторая команда не работает, поэтому использовался конвертатор svg to png (https://svgtopng.com/).

Процесс выглядит следующим образом:
![ML DAG](./images/dag_main.png)


## Замечания
В логах snakemake traceback-и не отображаются, поэтому ошибку будет очень сложно найти - будет только правило, которое его сгенерировало.

В правиле read и скрипте scripts/read_data.py можно посмотреть особенности указания input/output в скриптах и напрямую в правиле.

Гораздо больше пользы в документации - https://snakemake.readthedocs.io/en/v3.10.0/getting_started/examples.html.

Wildcard {sample}.csv в правиле transform1 определяется исходя из input в правиле all.


## License
Продумать

## Project status
В разработке
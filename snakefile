TRANSFORMS = ['1', '2']
FITS = ['1', '2']

rule sort:
    input:
        "data_in/dataset1.txt"
    output:
        "data_out/dataset1_sorted.txt"
    shell:
        "sort {input} > {output}"

rule all:
    input:
        expand('data_out/ml_trandformed{transform}.csv', transform = TRANSFORMS),
        expand('data_out/ml_fit{fit}{transform}.pkl', fit = FITS, transform = TRANSFORMS)

rule read:
    input:
        "data_in/ml.csv"
    output:
        "data_out/ml.csv"
    run: 
        import pandas as pd
        for i in range(len(input)):
            data = pd.read_csv(input[i])
            data.to_csv(output[i], index=False)

rule transform1:
    input:
        "data_out/{sample}.csv"
    output:
        "data_out/{sample}_trandformed1.csv"
    script:
        "scripts/preprocess1.py"

rule transform2:
    input:
        "data_out/ml.csv"
    output:
        "data_out/ml_trandformed2.csv"
    script:
        "scripts/preprocess2.py"

rule fit1:
    input:
        expand('data_out/ml_trandformed{transform}.csv', transform = TRANSFORMS)
    output:
        expand('data_out/ml_fit1{transform}.pkl', transform = TRANSFORMS)
    threads: 4
    resources: mem_mb=100
    script:
        "scripts/fit1.py"

rule fit2:
    input:
        expand('data_out/ml_trandformed{transform}.csv', transform = TRANSFORMS),
    output:
        expand('data_out/ml_fit2{transform}.pkl', transform = TRANSFORMS)
    threads: 4
    resources: mem_mb=100
    script:
        "scripts/fit2.py"